% A Framework for Comparing Models  
for Adaptive Testing
% Jill-Jênn Vie
% February 19, 2016

# Models for Adaptive Testing

## Computerized Adaptive Testing (CAT)

Asking the right questions to the right people.

![An adaptive test.](tree.pdf)

## First of all

### Assumptions

- Dichotomous items (either answered correctly or incorrectly)
- We do not care about item exposure (yet)

### Goals

- We want to ask \alert{as few questions as possible} in a test.
- Lots of different models. Which ones fit our data the most?

## 1. Rasch Model (`catR`)

![Example of CAT using the Rasch model.](irt.pdf)

## An example of CAT simulated with `catR`

    We ask question 42 to the examinee.
    Correct!
    We ask question 48 to the examinee.
    Correct!
    We ask question 82 to the examinee.
    Incorrect.
    We ask question 53 to the examinee.
    Correct!
    We ask question 78 to the examinee.
    Incorrect.
    We ask question 56 to the examinee.
    Correct!
    We ask question 76 to the examinee.
    Incorrect.
    We ask question 58 to the examinee.
    Incorrect.

## RPy2: R bindings for Python

```python
from rpy2.robjects import r
r('library(catR)')
r('one <- sample(1, 100, T)')
r('itembank <- cbind(one, c(1:100)/100, 1 - one, one)')
pattern = [1, 1, 0, 1, 0, 1, 0, 0]
ql = [42]
for t in range(len(pattern)):
    print('We ask question %d to the examinee.' % ql[-1])
    print('Correct!' if pattern[t] else 'Incorrect.')
    questions = ','.join(map(str, ql))
    answers = ','.join(map(str, pattern[:t + 1]))
    r('theta <- thetaEst(matrix(itembank[c(%s),],'
      'nrow=%d), c(%s))' % (questions, t + 1, answers))
    q = r('nextItem(itembank, NULL, theta, x = c(%s),'
          'out = c(%s))$item' % (answers, questions))[0]
    ql.append(q)
```

## 2. Cognitive Diagnosis (`CDM`) aka Rule-Space Method

Mapping knowledge components (KC) to items in order to diagnose \alert{misconceptions}.

. . .

### Example

- Solving Item 1 requires mastering KC 1 and 2 (or guessing)
- Solving Item 2 requires mastering KC 3
- ...

At the end of the test, we can provide a \alert{feedback} to the examinee.

## Example: DINA model aka q-matrix

DINA: Deterministic Input, Noisy “And” gate.

\begin{columns}[t]
\begin{column}{0.5\textwidth}
\[ \frac23 + \frac56 =\,? \]

\begin{description}
\item[KC 1] Put at same denominator
\item[KC 2] Add two fractions of same denominator
\end{description}

\end{column}
\begin{column}{0.5\textwidth}
\[ \frac12 \times \frac34 =\,? \]

\begin{description}
\item[KC 3] Multiply two fractions
\end{description}

\end{column}
\end{columns}

\vspace{1cm}

We can provide \alert{useful feedback} to examinees:

- ``You seem to have KC 2 and KC 3 but not KC 1.''

. . .

… Sorry, I said \alert{useful}:

- ``You seem to be able to add two fractions of same denominator and multiply two fractions, but not put two fractions at the same denominator.''

## Note: You may not find the DINA model on Google

![Another DINA model.](dina.png)

## What does a CD-CAT look like?

Cognitive Diagnosis Computerized Adaptive Testing.

    Round 1 -> We ask question 9 to the examinee.
    It requires KC: [0, 1, 0, 0, 0, 0, 0, 0]
    Correct!
    Examinee: [0.5, 0.74, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5]
    Estimate: 00000101100000000000
       Truth: 00011111111101001111

    Round 2 -> We ask question 6 to the examinee.
    It requires KC: [0, 0, 0, 0, 0, 0, 1, 0]
    Correct!
    Examinee: [0.5, 0.74, 0.5, 0.5, 0.5, 0.5, 0.91, 0.5]
    Estimate: 00000101100101010000
       Truth: 00011111111101001111

## What does a CD-CAT look like?

    Round 4 -> We ask question 2 to the examinee.
    It requires KC: [0, 0, 0, 1, 0, 0, 1, 0]
    Incorrect.
    Examinee: [0.5, 0.74, 0.5, 0.06, 0.5, 0.5, 0.96, 0.87]
    1 1 0 3 3 9 3 8 6 3 4 8 0 7 4 7 3 3 1 2
    Estimate: 00000101100101010000
       Truth: 00011111111101001111

    Round 6 -> We ask question 10 to the examinee.
    It requires KC: [0, 1, 0, 0, 1, 0, 1, 1]
    Correct!
    Examinee: [0.5, 0.99, 0.67, 0.06, 0.98, 0.5, 1.0, 0.99]
    Estimate: 00010101111101011001
       Truth: 00011111111101001111

## 3. Regression Trees (Yan, Lewis, Stocking)

![CAT using regression trees.](regression-trees.png)

## And many more

### Multidimensional Item Response Theory

$d$ latent traits instead of 1

### MIRT + q-matrix

Measure one latent model per knowledge component

### SPARFA: Sparse factor analysis

No access to full response patterns

### Multistage testing

Asking questions $k$ by $k$ instead of one by one\vspace{7mm}

. . .

\centering But \alert{how to compare them?}

## They're all flowcharts! (Or binary decision trees.)

![A binary decision tree.](tree.pdf)

# Framework, Experiment, Results

## Train/test datasets for both users and questions

- We train our models using a \alert{train} dataset of student response patterns
- We evaluate them on models the following way:
    - We ask questions with the same criterion for all models (MFI)
    - And keep a validation question set.

\centering\includegraphics[width=0.5\linewidth]{tree.pdf}

## Methods needed

- `training_step` over `train` dataset
- `init_test`
- `next_item` using questions and answers got so far
- `estimate_parameters` based on the last answer
- `predict_performance` of the model over the `validation_question_set`

. . .

### Example: `mirt.py` calling `mirtCAT` package

```python
def next_item(self, replied_so_far, results_so_far):
    next_item_id = mirtCAT.findNextItem(r.CATdesign)[0]
    return next_item_id - 1

def estimate_parameters(self, rep_so_far, res_so_far):
    r('CATdesign <- updateDesign(CATdesign, items=...)')
    r('CATdesign$person$Update.thetas(CATdesign$design)')
```

## Double cross-validation

![This is **not** a Belgian chocolate box.](crossval.pdf)

## Datasets

### SAT test: 296 students, 40 questions

Multidisciplinary: Mathematics, Biology, World History, French.  

### Fraction subtraction test: 536 students, 20 questions

KCs specified (*add fractions of same denominator*, etc.).  

## Results for the Fraction dataset: mean prediction error (negative log-likelihood)

\centering
![Negative log-likelihood over the validation question set after a certain number of questions.](fraction.png)\ 

## Results for the Fraction dataset: mean number of questions predicted correctly

\centering
![Number of questions predicted succesfully over the validation question set (5) after a certain number of questions.](counter.png)\ 

## Discussion

### Remarks

- After only 4 questions over 15, MIRT + q-matrix can predict correctly 4 out of 5
- Q-matrix (DINA) alone takes a long time to converge because first questions measure single KC
- In the early stages, Rasch Model performs well compared to 8-dim MIRT

### Future work

- How to compare a flowchart with the optimal flowchart?
- A q-matrix is expensive to build. How helpful is it?
- How to compare CAT with MST?

# NEW! Adaptive Submodularity

## Adaptive Submodularity (Golovin and Krause, 2010)

### Automated diagnosis

> Suppose we have different hypotheses about the state of a patient, and can run medical tests to rule out inconsistent hypotheses. The goal is to adaptively choose tests to infer the state of the patient as quickly as possible.

This can be seen as a \alert{Stochastic Set Cover} problem: we want to cover as many fake hypotheses as possible.

### Adaptive submodular function

$\sim$ convexity over discrete domains (= subsets of items).

If the function to maximize (= information) has a certain property (*monotonic submodular*), a \alert{greedy flowchart} builds a satisfying set: $(1 - 1 / e) \simeq 67\%$ of the optimal flowchart in average.

## Example 1: Vitamin C

Orange Apple Mango  Banana Lemon
------ ----- ------ ------ -----
51 mg  8 mg  122 mg 10 mg  31 mg

- We want to find the \alert{subset of $k$ fruits} having biggest vitamin C.
- But vitamin C is an additive function: $vitamin(\{banana, apple\}) = vitamin(\{banana\}) + vitamin(\{apple\})$
- Thus, taking the \alert{best fruit at each step} is optimal.

## What can be done with more generic functions?

$$f : 2^{E \times O} \to \mathbb{R}_{\geq 0}$$ is a function over subsets of pairs $(item, outcome)$.

### Monotonicity

> The marginal benefit of selecting an item is always nonnegative

### Submodularity

> Selecting an item later never increases its marginal benefit

### Our application

Any information function is supposed to be monotonic.  
Submodularity is a stronger assumption: one can discuss.

## Example 2: Maximizing Fisher information

- We want to compare `catR`'s flowchart of depth $k$ using MFI criterion with the optimal flowchart (achieving maximal Fisher information at the leaves).
- If the Fisher information function is monotone submodular,
- `catR`'s greedy algorithm taking best item for MFI criterion performs in average $(1 -  1 / e) \simeq 67\%$ as good as the best adaptive test. Good job David!

## Thanks for listening!

\includegraphics[width=4.2cm]{jiji.jpg}  
Jill-Jênn Vie\vspace{1cm}

\url{jiji.cat}  
\url{http://github.com/jilljenn}

\url{jjv@lri.fr}

If you're interested in adapting a script for your uses, please drop me an issue :)
